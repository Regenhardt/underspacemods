﻿using UnityEngine.SceneManagement;
using UnityEngine;
using MelonLoader;
using System.Diagnostics;
using System;

[assembly: MelonGame("Pastaspace Interactive", "Underspace")]
[assembly: MelonInfo(typeof(TestMod.SpaceLogger), "LogMod", "0.0.1", "Metallkiller")]

namespace TestMod
{
    public class SpaceLogger : MelonMod
    {
        public GameObject shipMain;

        // look into the melon docs but there are a tonne of prebuilt methods you can utilize like on initialize 
        public override void OnInitializeMelon()
        {
            LoggerInstance.Msg("Mod Loaded: Press Space In Game.");
        }

        public override void OnUpdate()
        {
            // you can utilize any unity methods as long as you refrence the .dll they belong too
            Scene currentScene = SceneManager.GetActiveScene();
            string sceneName = currentScene.name;

            if (sceneName == "MainGame")
            {
                if (shipMain == null || GameObject.Find(shipMain.name) == null)
                {
                    // get ship object
                    GameObject[] obj = GameObject.FindObjectsOfType<GameObject>();
                    foreach (GameObject g in obj)
                    {
                        if(g.IsPlayerShip())
                        {
                            shipMain = g;
                            LoggerInstance.Msg(g.name);
                        }
                    }
                }

                if (Input.GetKeyDown(KeyCode.Space))
                {
                    // Melons version of Debug.Log
                    LoggerInstance.Msg("Main Scene Loaded");
                    LoggerInstance.Msg("Key Pressed");
                    LoggerInstance.Msg($"Player ship name: {shipMain?.name}");
                    LoggerInstance.Msg("Throwing exception");
                    // Runtime information
                    LoggerInstance.Msg($"Current Scene: {sceneName}");
                    LoggerInstance.Msg($"dotnet runtime: {Environment.Version}");
                    LoggerInstance.Msg($"OS: {Environment.OSVersion}");
                    // Unity version
                    LoggerInstance.Msg($"Unity Version: {Application.unityVersion}");
                    // Process information
                    Process currentProcess = Process.GetCurrentProcess();
                    LoggerInstance.Msg($"Process Name: {currentProcess.ProcessName}");
                    LoggerInstance.Msg($"Process ID: {currentProcess.Id}");
                    
                    throw new System.Exception("I'm flying!");
                }
            }
        }
    }

    public static class GameObjectExtensions
    {
        // this pretends it's an instance method
        public static bool IsPlayerShip(this GameObject o)
        {
            return o.name.Contains("(Clone)") &&
                o.name.Contains("Fighter") || o.name.Contains("Freighter") || o.name.Contains("Bomber") &&
                !o.name.Contains("NPC");
        }
    }
}
