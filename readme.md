# Underspace mods

First try at modding Underspace. 

- Install [MelonLoader](https://melonwiki.xyz)
- Set environment variable `UNDERSPACE_DIR` to underspace location, e.g. `C:\Program Files (x86)\Steam\steamapps\common\Underspace`
- Build project
- Copy `TestMod.dll` to `UNDERSPACE_DIR\Mods`
- Run underspace